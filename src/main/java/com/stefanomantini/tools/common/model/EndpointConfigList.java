package com.stefanomantini.tools.common.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Component
@RefreshScope
@ConfigurationProperties("healthcheck")
public class EndpointConfigList {
  private String serviceName;
  private Integer interval;
  private Integer timeout;
  private List<EndpointConfig> probes;
}
