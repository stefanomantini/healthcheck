package com.stefanomantini.tools.service.contract;

import com.stefanomantini.tools.common.model.EndpointConfig;
import com.stefanomantini.tools.common.model.Probe;
import java.util.Map;
import java.util.stream.Stream;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProbeService {

  /**
   * Applies logic around code matching
   *
   * @return
   */
  Stream<Probe> getProbes();

  Flux<ResponseEntity<String>> aggregateHealthChecksReactive();

  /**
   * Perform Healthcheck
   *
   * <p>Scheduling work on Schedulers.parallel is counter-productive here; the parallel scheduler is
   * made for CPU-bound work, whereas making http calls is likely to be latency-bound. Parallelism
   * happens naturally with reactive streams, since the consumer request a number of items depending
   * on its capacity
   *
   * @return
   */
  Map<EndpointConfig, Probe> aggregateHealthChecks();

  /**
   * Simple string building utility
   *
   * @param protocol
   * @param host
   * @param port
   * @param endpoint
   * @return
   */
  String buildEndpoint(String protocol, String host, Integer port, String endpoint);

  String buildEndpoint(EndpointConfig ec);

  /**
   * peforms an individual http request
   *
   * @param endpoint
   * @return
   */
  Mono<ClientResponse> getClientResponse(String endpoint);
}
