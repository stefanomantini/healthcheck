package com.stefanomantini.tools.service.impl;

import com.stefanomantini.tools.common.enumeration.HealthStatus;
import com.stefanomantini.tools.common.model.EndpointConfig;
import com.stefanomantini.tools.common.model.EndpointConfigList;
import com.stefanomantini.tools.common.model.Probe;
import com.stefanomantini.tools.service.contract.ProbeService;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Service
public class ProbeServiceImpl implements ProbeService {

  private static final Integer SOCKET_TIMEOUT = 1000;
  @Autowired EndpointConfigList endpointConfigList;

  private final WebClient webClient =
      WebClient.builder().defaultHeader("Content-Type", "application/json").build();

  private final AsyncRestTemplate rt = new AsyncRestTemplate();

  /**
   * TODO refactor
   *
   * @return
   */
  @Override
  public Stream<Probe> getProbes() {
    List<Probe> probeList = new ArrayList<>();

    for (Map.Entry<EndpointConfig, Probe> re : this.aggregateHealthChecks().entrySet()) {
      probeList.add(re.getValue());
    }
    return probeList.stream();
  }

  /**
   * TODO check this preserves order...
   *
   * @return
   */
  @Override
  public Flux<ResponseEntity<String>> aggregateHealthChecksReactive() {
    return Flux.fromIterable(this.endpointConfigList.getProbes())
        .flatMap(
            e ->
                this.webClient
                    .get()
                    .uri(this.buildEndpoint("http", e.getHost(), e.getPort(), e.getEndpoint()))
                    .exchange()
                    .flatMap(response -> response.toEntity(String.class))
                    .onErrorResume(f -> this.connectFailure())
                    .flatMap(entity -> Mono.just(entity)))
        .subscribeOn(Schedulers.parallel());
  }

  /**
   * TODO rewrite as reactive
   *
   * @return
   */
  @Deprecated
  @Override
  public Map<EndpointConfig, Probe> aggregateHealthChecks() {
    Map<EndpointConfig, Probe> responseEntityMap = new LinkedHashMap<>();
    for (EndpointConfig endpointConfig : this.endpointConfigList.getProbes()) {
      responseEntityMap.put(endpointConfig, this.executeProbe(endpointConfig));
    }
    return responseEntityMap;
  }

  private Probe executeProbe(EndpointConfig endPointConfig) {

    ResponseEntity re;
    String url =
        this.buildEndpoint(
            "http",
            endPointConfig.getHost(),
            endPointConfig.getPort(),
            endPointConfig.getEndpoint());
    Long start = Instant.now().toEpochMilli();
    Long end;

    try {
      ListenableFuture<ResponseEntity<String>> future =
          this.rt.exchange(url, HttpMethod.GET, null, String.class);
      re = future.get(this.endpointConfigList.getTimeout(), TimeUnit.SECONDS);
      end = Instant.now().toEpochMilli();
    } catch (Exception e) {
      re = ResponseEntity.status(HttpStatus.GATEWAY_TIMEOUT).body("");
      end = Instant.now().toEpochMilli();
    }
    return this.mapResponseEntityToProbe(re, end - start, url, endPointConfig.getSuccessCode());
  }

  private Probe mapResponseEntityToProbe(
      ResponseEntity re, Long duration, String url, Integer successCode) {
    return Probe.builder()
        .responseTime(duration)
        .status(successCode == re.getStatusCodeValue() ? HealthStatus.UP : HealthStatus.DOWN)
        .expectedResponseStatus(HttpStatus.valueOf(successCode))
        .responseStatus(re.getStatusCode())
        .endpoint(url)
        .build();
  }

  /**
   * Default fallback method for connect failure
   *
   * @return
   */
  private Mono<ResponseEntity<String>> connectFailure() {
    return Mono.just(new ResponseEntity(HttpStatus.BAD_GATEWAY));
  }

  @Override
  public String buildEndpoint(String protocol, String host, Integer port, String endpoint) {
    return new StringBuilder()
        .append(protocol)
        .append("://")
        .append(host)
        .append(":")
        .append(port)
        .append("/")
        .append(endpoint)
        .toString();
  }

  @Override
  public String buildEndpoint(EndpointConfig ec) {
    return this.buildEndpoint("http", ec.getHost(), ec.getPort(), ec.getEndpoint());
  }

  @Override
  public Mono<ClientResponse> getClientResponse(String endpoint) {
    return this.configureWebClient().baseUrl(endpoint).build().get().exchange();
  }

  /**
   * we do long-term want to do this on a per-request basis?
   *
   * @return
   */
  private WebClient.Builder configureWebClient() {
    TcpClient timeoutClient =
        TcpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, this.endpointConfigList.getTimeout() * 10)
            .option(ChannelOption.SO_TIMEOUT, SOCKET_TIMEOUT)
            .doOnConnected(
                c ->
                    c.addHandlerLast(new ReadTimeoutHandler(this.endpointConfigList.getTimeout()))
                        .addHandlerLast(
                            new WriteTimeoutHandler(this.endpointConfigList.getTimeout())));
    return WebClient.builder()
        .clientConnector(new ReactorClientHttpConnector(HttpClient.from(timeoutClient)));
  }
}
