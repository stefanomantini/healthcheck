package com.stefanomantini.tools.web.contract;

import com.stefanomantini.tools.common.model.Probe;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import reactor.core.publisher.Flux;

@Api("Probes")
public interface ProbeResourceContract {

  @ApiOperation(
      value = "Retrieves Probe information",
      notes = "This endpoint returns Probe information for registered probes")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Probes retrieved successfully"),
    @ApiResponse(code = 500, message = "Error while processing the request")
  })
  public Flux<Probe> getProbes();
}
